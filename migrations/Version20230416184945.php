<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230416184945 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__book AS SELECT id, title, subtitle, published_at, isbn, language, cover, pages, description FROM book');
        $this->addSql('DROP TABLE book');
        $this->addSql('CREATE TABLE book (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, subtitle VARCHAR(255) DEFAULT NULL, published_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , isbn VARCHAR(13) NOT NULL, language VARCHAR(255) NOT NULL, cover VARCHAR(255) DEFAULT NULL, pages INTEGER NOT NULL, description CLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO book (id, title, subtitle, published_at, isbn, language, cover, pages, description) SELECT id, title, subtitle, published_at, isbn, language, cover, pages, description FROM __temp__book');
        $this->addSql('DROP TABLE __temp__book');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__book AS SELECT id, title, subtitle, published_at, isbn, language, cover, pages, description FROM book');
        $this->addSql('DROP TABLE book');
        $this->addSql('CREATE TABLE book (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, subtitle VARCHAR(255) DEFAULT NULL, published_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , isbn VARCHAR(13) NOT NULL, language VARCHAR(255) NOT NULL, cover VARCHAR(255) NOT NULL, pages INTEGER NOT NULL, description CLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO book (id, title, subtitle, published_at, isbn, language, cover, pages, description) SELECT id, title, subtitle, published_at, isbn, language, cover, pages, description FROM __temp__book');
        $this->addSql('DROP TABLE __temp__book');
    }
}
